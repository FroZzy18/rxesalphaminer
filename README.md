# RXESAlphaMiner#

RXESAlphaMiner is an application that takes imperative ordering relations used by the Alpha miner process mining algorithm as an input and generates the corresponding process model by applying the Alpha algorithm. The ordering relations have to be provided as a structured CSV file that is the output of another tool I developed called RXESuite (https://bitbucket.org/FroZzy18/rxesuite). The application was developed as part of the Demonstration section of my Master thesis at the Vienna University of Economics and Business (WU Wien).

### How do I get set up? ###

The repository contains all the necessary source code and library files for compiling the application. The application has to be compiled with the Java SE Development Kit 8 for a respective platform.

It is important to stress that this application can at the moment only be used in combination with RXESuite, since it requires a specific structure of the ordering relations. There is a demonstration of RXESuite that is part of the RXESuite Readme where the ordering relations for the Alpha algorithm are exported. I continue this demonstration with this tool by showing how those relations in the structured CSV file can be used to generate a graphical process model by applying the Alpha algorithm.

I also provide a pre-compiled, ready-to-run version of the application which can be found in the demonstration folder of this repository. It is part of the overall demonstration-scenario of RXESuite. The demonstration continues here after the export of the CSV file of the Alpha algorithm ordering relations. 

### Demonstration ###

Requirements:

- Java SE Development Kit 8 or the Java SE Runtime Environment 8 needs to be installed
- CSV file with ordering relations for the Alpha algorithm exported from RXESuite (is already included in the demonstration folder)
- GraphViz needs to be installed (http://www.graphviz.org/Download.php)
- The GraphViz "TempDirectory" has to be in: "D:/Program Files (x86)/GraphViz/temp"
- The executeable dot.exe file of GraphViz has to be in: ""D:/Program Files (x86)/GraphViz/bin/dot.exe"
- At the moment this demonstration was only tested on Windows 10

GraphViz is used to "draw" the final process model by generating the model in a special language "DOT language" which is then used as an input for that specific GraphViz tool. The reason why strict paths are specified here is, that this application was primarily designed for demonstration purposes. I developed this tool primarily to show that it is possible to use the exported files of RXESuite to create graphical process models. Therefore, I hardcoded the path to GraphViz directly in the source code. However, for future versions of this tool it is easily possible to include a feature to allow the user to set the paths to the temp directory of GraphViz and to the dot.exe file directly in the application.

1. Download the repository (or just the demonstration folder) to your local harddrive.
2. Double click on RXESAlphaMiner.jar to start the application.
3. You should now see the main window of RXESAlphaMiner.
![Demonstration_RXESAlphaminer.PNG](https://bitbucket.org/repo/kaLd9y/images/2361239103-Demonstration_RXESAlphaminer.PNG)

4. Click on the "File..." button and select the alpha_relations.csv file. Either the one exported from RXESuite, or the file included in the demonstration folder.
5. Click on the "Mine Process Model" button to use the ordering relations contained in the alpha_relations.csv file to create a graphical process model. This process model is then directly shown in the center of the application.
![Demonstration_RXESAlphaminer_Model.PNG](https://bitbucket.org/repo/kaLd9y/images/2922612396-Demonstration_RXESAlphaminer_Model.PNG)´

The graphical process model is created by using the ordering relations for the Alpha algorithm and specifying the resulting model in the DOT language. Then, the model in the DOT language is used to draw a graphical model using GraphViz.
 

### What is the meaning of this? ###

This demonstration shows that it is possible to use the exported CSV files of RXESuite as an input for specialized process mining tools to create imperative process models. In the future, more applications could be developed that take the ordering relations for other imperative mining algorithms as an input and apply the respective algorithms to create process models.

For more information please refer to my Master thesis that probably will be available at the Vienna University of Economics and Business.