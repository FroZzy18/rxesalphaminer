package org.processmining.plugins.rxesalphaminer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import com.opencsv.CSVReader;

public class PowerSetPresenter implements IMainPresenter{

	IMainView view;
	
	public PowerSetPresenter(IMainView view){
		this.view = view;
	}
	
	public static <T> Set<Set<T>> powerSet(Set<T> originalSet) {
	    Set<Set<T>> sets = new HashSet<Set<T>>();
	    if (originalSet.isEmpty()) {
	    	sets.add(new HashSet<T>());
	    	return sets;
	    }
	    List<T> list = new ArrayList<T>(originalSet);
	    T head = list.get(0);
	    Set<T> rest = new HashSet<T>(list.subList(1, list.size())); 
	    for (Set<T> set : powerSet(rest)) {
	    	Set<T> newSet = new HashSet<T>();
	    	newSet.add(head);
	    	newSet.addAll(set);
	    	sets.add(newSet);
	    	sets.add(set);
	    }		
	    return sets;
	}
	
	public void generateModel(String filename){
		try {
			CSVReader reader = new CSVReader(new FileReader(filename), ';');
			String[] nextLine;
			reader.readNext();
			
			List<String> causalityRelation = new ArrayList<String>();
			List<String> concurrencyRelation = new ArrayList<String>();
			List<String> exclusivenessRelation = new ArrayList<String>();
			List<String> startingActivites = new ArrayList<String>();
			List<String> endingActivites = new ArrayList<String>();
			Set<String> activityNames = new HashSet<String>();

			while ((nextLine = reader.readNext()) != null) {
				activityNames.add(nextLine[0]);
				activityNames.add(nextLine[2]);
				if (nextLine[1].equals("->"))
						causalityRelation.add(nextLine[0]+";"+nextLine[2]);
				if (nextLine[1].equals("||"))
					concurrencyRelation.add(nextLine[0]+";"+nextLine[2]);
				if (nextLine[1].equals("#"))
					exclusivenessRelation.add(nextLine[0]+";"+nextLine[2]);
				if (nextLine[1].equals("start"))
					startingActivites.add(nextLine[0]);
				if (nextLine[1].equals("end"))
					endingActivites.add(nextLine[0]);
			}
			reader.close();
			
			List<Place> Xw = new ArrayList<Place>();
			List<String> Xa = new ArrayList<String>();
			List<String> Xb = new ArrayList<String>();
			List<String> Xc = new ArrayList<String>();
			Set<String> names = new HashSet<String>();
			Set<Set<String>> PowerSet = new HashSet<Set<String>>();
			
			for (String s : activityNames) {
				names.add(s);
			}
			
			PowerSet = powerSet(names);

			for (Set<String> from : PowerSet){
				if(from.isEmpty())
					continue;
				for (Set<String> to : PowerSet) {
					if(to.isEmpty())
						continue;
					boolean aok = true;
					boolean bok = true;
					for (String fromactivity : from){
						for (String toactivtiy: to) {
							if (!causalityRelation.contains(fromactivity+";"+toactivtiy))
							{
								aok = false;
								bok = false;
							}
							for (String a1 : from){
								for (String a2: from){
									if(!exclusivenessRelation.contains(a1+";"+a2))
											aok = false;
								}
							}
							for (String b1: to){
								for (String b2: to){
									if(!exclusivenessRelation.contains(b1+";"+b2))
											bok = false;
								}
							}
							
						}
					}
					if(aok && bok){
						Place p = new Place();
						for (String s : from){
							p.from.add(s);
						}
						for (String s : to){
							p.to.add(s);
						}
						Xw.add(p);
					}
				}
			}
			
			List<Place> Yw = new ArrayList<Place>(Xw);
			
			for(Place outer : Xw){
				for (Place inner : Xw){
					if (outer.from.size() >= inner.from.size() && outer.to.size() >= inner.to.size()){
						List tempListFrom = new ArrayList<String>(inner.from);
						List tempListTo = new ArrayList<String>(inner.to);
						boolean outerfrombigger = false;
						boolean outertobigger = false;
						for (String s : outer.from){
							if(tempListFrom.contains(s)){
								tempListFrom.remove(s);
							} else
								outerfrombigger = true;
						}
						for (String s : outer.to){
							if(tempListTo.contains(s)){
								tempListTo.remove(s);
							} else
								outertobigger = true;
						}
						if(tempListFrom.size() <= 0 && tempListTo.size() <= 0 && (outerfrombigger || outertobigger)){
							Yw.remove(inner);
						}
					}
				}
			}
			
			for(Place p : Yw){
				for (String s : p.from){
					System.out.println(s);
				}
				System.out.println("->");
				for (String s : p.to) {
					System.out.println(s);
				}
				System.out.println("-----------------");
			}
			
			
			Set<String> places = new HashSet<String>();
			places.add("\"start\"");
			places.add("\"end\"");
			for (Place p : Yw){
				places.add("\""+p.getPlaceName()+"\"");
			}
			
			String model = "digraph G { \n";
			
			for (Place p : Yw){
				model = model + "{ ";
				for (String s : p.from){
					model = model+"\""+s+"\" ";
				}
				model = model+"} -> \""+p.getPlaceName()+"\" -> { ";
				for (String s : p.to){
					model = model+"\""+s+"\" ";
				}
				model = model+"}";
			}
			
			for (String s : startingActivites){
				model = model + "start -> \""+s+"\"\n";
			}
			
			for (String s : endingActivites){
				model = model +"\""+ s +"\" -> end \n";
			}
			
			for (String s : activityNames){
				model = model +"\""+ s + "\" [shape = square]\n";
			}
			
			for (String s : places) {
				if (s.equals("\"start\"") || s.equals("\"end\""))
					model = model + s + "[shape = circle fontsize = 14]\n";
				else
					model = model + s + " [shape = circle fontsize = 7 label = \"\"]\n";
			}
			
			model = model + "rankdir = \"LR\" \n";
			model = model + "}";
			
			GraphViz gv = new GraphViz();
			gv.setDotSource(model);
			String type = "png";
			String representationType="dot";
			File out = new File("out."+type);
			gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), type, representationType), out);
			
			view.showModel(new ImageIcon(ImageIO.read(new File(out.getAbsolutePath()))));
			
		} catch (Exception e) {
			view.showError("File could not be processed!");
			e.printStackTrace();
		}
		
	}

}
