package org.processmining.plugins.rxesalphaminer;

public interface IMainPresenter {
	public void generateModel(String filename);
}
