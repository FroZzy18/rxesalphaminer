package org.processmining.plugins.rxesalphaminer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MainView implements IMainView{
	
	private IMainPresenter presenter;

	private JFrame frame = new JFrame("RXESAlphaMiner");
	private JPanel topControls = new JPanel();
	private JLabel RelationsFileLabel = new JLabel("Relations-File: ");
	private JButton selectFileButton = new JButton("File...");
	private JTextField RelationsFileTextField = new JTextField(50);
	private JButton generateModelButton = new JButton("Mine Process-Model");
	private JLabel picture = new JLabel();
	
	public MainView(){
		createUI();
	}
	
	public void setPresenter(IMainPresenter p){
		presenter = p;
	}
	
	public void createUI(){
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(700,400));
		frame.setLayout(new BorderLayout());
		
		selectFileButton.addActionListener((e) -> {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
			fileChooser.showOpenDialog(frame);
			File file = fileChooser.getSelectedFile();
			if (file != null) {
				RelationsFileTextField.setText(file.getAbsolutePath());
			}
		});
		
		generateModelButton.addActionListener((e) -> presenter.generateModel(RelationsFileTextField.getText()));
		
		topControls.add(RelationsFileLabel);
		topControls.add(RelationsFileTextField);
		topControls.add(selectFileButton);
		topControls.add(generateModelButton);
		
		frame.add(topControls, BorderLayout.NORTH);
		frame.add(picture, BorderLayout.CENTER);
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setLocationRelativeTo(null);
		frame.pack();
		frame.setVisible(true);
	}
	
	
	public void showModel(ImageIcon model) {
		picture.setIcon(model);
		picture.setHorizontalAlignment(JLabel.CENTER);;
		picture.setVisible(true);
		frame.pack();
	}
	
	public void showError(String errorMessage) {
		JOptionPane.showMessageDialog(frame, errorMessage, "Error!", JOptionPane.WARNING_MESSAGE);
	}

}
