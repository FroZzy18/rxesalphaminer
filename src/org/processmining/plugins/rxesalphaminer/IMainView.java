package org.processmining.plugins.rxesalphaminer;

import javax.swing.ImageIcon;

public interface IMainView {
	public void showModel(ImageIcon model);
	public void showError(String errorMessage);
}
