package org.processmining.plugins.rxesalphaminer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import com.opencsv.CSVReader;

public class SimplePresenter implements IMainPresenter{

	IMainView view;
	
	public SimplePresenter(IMainView view){
		this.view = view;
	}
	
	public void generateModel(String filename){
		try {
			CSVReader reader = new CSVReader(new FileReader(filename), ';');
			String[] nextLine;
			reader.readNext();
			
			List<String> causalityRelation = new ArrayList<String>();
			List<String> concurrencyRelation = new ArrayList<String>();
			List<String> exclusivenessRelation = new ArrayList<String>();
			List<String> startingActivites = new ArrayList<String>();
			List<String> endingActivites = new ArrayList<String>();
			Set<String> activityNames = new HashSet<String>();

			while ((nextLine = reader.readNext()) != null) {
				activityNames.add(nextLine[0]);
				activityNames.add(nextLine[2]);
				if (nextLine[1].equals("->"))
						causalityRelation.add(nextLine[0]+";"+nextLine[2]);
				if (nextLine[1].equals("||"))
					concurrencyRelation.add(nextLine[0]+";"+nextLine[2]);
				if (nextLine[1].equals("#"))
					exclusivenessRelation.add(nextLine[0]+";"+nextLine[2]);
				if (nextLine[1].equals("start"))
					startingActivites.add(nextLine[0]);
				if (nextLine[1].equals("end"))
					endingActivites.add(nextLine[0]);
			}
			reader.close();
			
			List<String> Xa = new ArrayList<String>();
			List<String> Xb = new ArrayList<String>();
			List<String> Xc = new ArrayList<String>();
			
			Xa.addAll(causalityRelation);
			
			for (String activity : activityNames) {
				List<String> causalActivities = new ArrayList<String>();
				for (String relation : causalityRelation){
					if(relation.split(";")[0].equals(activity)){
						causalActivities.add(relation.split(";")[1]);
					}
				}
				for (String i : causalActivities){
					for (String j : causalActivities) {
						if (!i.equals(j)){
							if (exclusivenessRelation.contains(i+";"+j)){
								if(!Xb.contains(activity+";"+j+";"+i))
									Xb.add(activity+";"+i+";"+j);
							}
						}
					}
				}
			}
			
			for (String exclusiveRelation : exclusivenessRelation) {
				String a = exclusiveRelation.split(";")[0];
				String b = exclusiveRelation.split(";")[1];
				
				if (!a.equals(b)){
					for(String activity : activityNames){
						if(causalityRelation.contains(a+";"+activity) && causalityRelation.contains(b+";"+activity)){
							if(!Xc.contains(b+";"+a+";"+activity))
								Xc.add(a+";"+b+";"+activity);
						}
					}
				}
			}
			
			for (String relation : Xb){
				String[] split = relation.split(";");
				String a = split[0];
				String b = split[1];
				String c = split[2];
				
				Xa.remove(a+";"+b);
				Xa.remove(a+";"+c);
			}
			
			for (String relation : Xc){
				String[] split = relation.split(";");
				String b = split[0];
				String c = split[1];
				String d = split[2];
				
				Xa.remove(b+";"+d);
				Xa.remove(c+";"+d);
			}
			
			Set<String> places = new HashSet<String>();
			places.add("\"start\"");
			places.add("\"end\"");
			
			String model = "digraph G { \n";
			
			for (String s : Xa){
				String[] split = s.split(";");
				String a = split[0];
				String b = split[1];
				
				model = model +"\""+ a  +"\" -> \"p("+a+";"+b+")\" -> \""+b+"\"\n";
				places.add("\"p("+a+";"+b+")\"");
			}
			
			for (String s : Xb){
				String[] split = s.split(";");
				String a = split[0];
				String b = split[1];
				String c = split[2];
				
				model = model + "\""+ a +"\" -> \"p("+a+">"+b+"#"+c+")\" -> {\""+b+"\" \""+c+"\"} \n";
				places.add("\"p("+a+">"+b+"#"+c+")\"");
			}
			
			for (String s : Xc){
				String[] split = s.split(";");
				String a = split[0];
				String b = split[1];
				String c = split[2];
				
				model = model + "{\""+a+"\" \""+b+"\"} -> \"p("+a+"#"+b+">"+c+")\" -> \""+c+"\" \n";
				places.add("\"p("+a+"#"+b+">"+c+")\"");
			}
			
			for (String s : startingActivites){
				model = model + "start -> \""+s+"\"\n";
			}
			
			for (String s : endingActivites){
				model = model +"\""+ s +"\" -> end \n";
			}
			
			for (String s : activityNames){
				model = model +"\""+ s + "\" [shape = square]\n";
			}
			
			for (String s : places) {
				if (s.equals("\"start\"") || s.equals("\"end\""))
					model = model + s + "[shape = circle fontsize = 14]\n";
				else
					model = model + s + " [shape = circle fontsize = 7 label = \"\"]\n";
			}
			
			model = model + "rankdir = \"LR\" \n";
			model = model + "}";
			
			GraphViz gv = new GraphViz();
			gv.setDotSource(model);
			String type = "png";
			String representationType="dot";
			File out = new File("out."+type);
			gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), type, representationType), out);
			
			view.showModel(new ImageIcon(ImageIO.read(new File(out.getAbsolutePath()))));
			
		} catch (Exception e) {
			view.showError("File could not be processed!");
			e.printStackTrace();
		}
		
	}

}
