package org.processmining.plugins.rxesalphaminer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import com.opencsv.CSVReader;

public class MainPresenter implements IMainPresenter{

	IMainView view;
	
	public MainPresenter(IMainView view){
		this.view = view;
	}
	
	public boolean isIdenticalHashSet (Set<String> h1, Set<String> h2) {
	    if ( h1.size() != h2.size() ) {
	        return false;
	    }
	    HashSet<String> clone = new HashSet<String>(h2); // just use h2 if you don't need to save the original h2
	    for (String A : h1){
	        if (clone.contains(A)){ // replace clone with h2 if not concerned with saving data from h2
	            clone.remove(A);
	        } else {
	            return false;
	        }
	    }
	    return true; // will only return true if sets are equal
	}
	
	
	public void generateModel(String filename){
		try {
			CSVReader reader = new CSVReader(new FileReader(filename), ';');
			String[] nextLine;
			reader.readNext();
			
			List<String> causalityRelation = new ArrayList<String>();
			List<String> concurrencyRelation = new ArrayList<String>();
			List<String> exclusivenessRelation = new ArrayList<String>();
			List<String> startingActivites = new ArrayList<String>();
			List<String> endingActivites = new ArrayList<String>();
			Set<String> activityNames = new HashSet<String>();

			while ((nextLine = reader.readNext()) != null) {
				activityNames.add(nextLine[0]);
				activityNames.add(nextLine[2]);
				if (nextLine[1].equals("->"))
						causalityRelation.add(nextLine[0]+";"+nextLine[2]);
				if (nextLine[1].equals("||"))
					concurrencyRelation.add(nextLine[0]+";"+nextLine[2]);
				if (nextLine[1].equals("#"))
					exclusivenessRelation.add(nextLine[0]+";"+nextLine[2]);
				if (nextLine[1].equals("start"))
					startingActivites.add(nextLine[0]);
				if (nextLine[1].equals("end"))
					endingActivites.add(nextLine[0]);
			}
			reader.close();
			
			List<Place> Xw = new ArrayList<Place>();
			Set<String> names = new HashSet<String>();
			
			for (String s : activityNames) {
				names.add(s);
			}
			for (String outer:causalityRelation){
				
					Place p = new Place();
					p.from.add(outer.split(";")[0]);
					p.to.add(outer.split(";")[1]);
					Xw.add(p);
				
			}
		
			for (String activityname : activityNames){ //Check if the left side is equal, and the right side is exclusive
				Set<String> causalActivities = new HashSet<String>();
				for (String causalRelation : causalityRelation){
						if(causalRelation.split(";")[0].equals(activityname)){
							causalActivities.add(causalRelation.split(";")[1]);
						}
				}
				Set<String> causalAndExclusiveActivities = new HashSet<String>();
				for (String a : causalActivities){
					for (String b: causalActivities) {
						if(exclusivenessRelation.contains(a+";"+b) && !a.equals(b)) {
								causalAndExclusiveActivities.add(a);
								causalAndExclusiveActivities.add(b);
						}
					}
				} if(!causalAndExclusiveActivities.isEmpty()) {
					Place p = new Place();
					p.from.add(activityname);
					for (String s : causalAndExclusiveActivities){
						p.to.add(s);
					} 
					Xw.add(p);
				}
			}
			
			List<Place> Yw = new ArrayList<Place>(Xw);
			
			for (Place p : Xw){ //Check if the right side is identical, and the left side is exclusive
				inner:
				for (Place p2: Xw) {
					Set<String> fromActivities = new HashSet<String>();
					if(isIdenticalHashSet(p.to, p2.to)){
						for(String s : p.from){
							for (String s2 : p2.from){
								if(!exclusivenessRelation.contains(s+";"+s2))
									continue inner;
							}
						}
						for(String s: p.from){
							fromActivities.add(s);
						}
						for(String s:p2.from){
							fromActivities.add(s);
						}
						Place n = new Place();
						n.to.addAll(p.to);
						for (String s:fromActivities){
							n.from.add(s);
						}
						for (Place e : Yw){
							if(isIdenticalHashSet(e.from, n.from) && isIdenticalHashSet(e.to, n.to)){
								continue inner;
							}
						}
							Yw.add(n);
					}
				}
			}
					
			List<Place> Zw = new ArrayList<Place>(Yw);
			
			for(Place outer : Yw){	// only keep bigger sets if bigger and smaller sets exist
				for (Place inner : Yw){
					if (outer.from.size() >= inner.from.size() && outer.to.size() >= inner.to.size()){
						List<String> tempListFrom = new ArrayList<String>(inner.from);
						List<String> tempListTo = new ArrayList<String>(inner.to);
						boolean outerfrombigger = false;
						boolean outertobigger = false;
						for (String s : outer.from){
							if(tempListFrom.contains(s)){
								tempListFrom.remove(s);
							} else
								outerfrombigger = true;
						}
						for (String s : outer.to){
							if(tempListTo.contains(s)){
								tempListTo.remove(s);
							} else
								outertobigger = true;
						}
						if(tempListFrom.size() <= 0 && tempListTo.size() <= 0 && (outerfrombigger || outertobigger)){
							Zw.remove(inner);
						}
					}
				}
			}
			
			/*
			 * Code to print the resulting relationships in an easy to analyse way
			 * 
			for(Place p : Zw){
				for (String s : p.from){
					System.out.println(s);
				}
				System.out.println("->");
				for (String s : p.to) {
					System.out.println(s);
				}
				System.out.println("-----------------");
			}
			*/
			
			
			Set<String> places = new HashSet<String>();
			places.add("\"start\"");
			places.add("\"end\"");
			for (Place p : Zw){ //Create Places Set to configure the shape of the nodes afterwards
				places.add("\""+p.getPlaceName()+"\"");
			}
			
			String model = "digraph G { \n";
			
			for (String s : startingActivites){
				model = model + "start -> \""+s+"\"\n";
			}
			
			
			for (Place p : Zw){ // Add the places from Zw
				model = model + "{ ";
				for (String s : p.from){
					model = model+"\""+s+"\" ";
				}
				model = model+"} -> \""+p.getPlaceName()+"\" -> { ";
				for (String s : p.to){
					model = model+"\""+s+"\" ";
				}
				model = model+"}";
			}
			
			for (String s : endingActivites){
				model = model +"\""+ s +"\" -> end \n";
			}
			
			for (String s : activityNames){	//Set the shape of activities to squares
				model = model +"\""+ s + "\" [shape = square]\n";
			}
			
			for (String s : places) { //Define the shape of the places
				if (s.equals("\"start\"") || s.equals("\"end\""))
					model = model + s + "[shape = circle fontsize = 14]\n";
				else
					model = model + s + " [shape = circle fontsize = 7 label = \"\"]\n";
			}
			
			model = model + "rankdir = \"LR\" \n";
			model = model + "}";
			
			GraphViz gv = new GraphViz();
			gv.setDotSource(model);
			String type = "png";
			String representationType="dot";
			File out = new File("out."+type);
			gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), type, representationType), out);
			
			view.showModel(new ImageIcon(ImageIO.read(new File(out.getAbsolutePath()))));
			
		} catch (Exception e) {
			view.showError("File could not be processed!");
			e.printStackTrace();
		}
		
	}

}
