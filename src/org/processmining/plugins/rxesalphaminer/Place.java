package org.processmining.plugins.rxesalphaminer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Place {

	public Set<String> from = new HashSet<String>();
	public Set<String> to = new HashSet<String>();
	
	public String getPlaceName(){
		String name = "p(";
		for (String s : from){
			name=name+s+";";
		}
		name = name.substring(0, name.length()-1);
		name = name +">";
		for (String s : to){
			name=name+s+";";
		}
		name = name.substring(0, name.length()-1);
		return name;
	}
}
